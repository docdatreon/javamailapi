import com.sun.mail.iap.ConnectionException;
import javax.mail.MessagingException;

public class Main {

    public static void main(String[] args) throws ConnectionException, MessagingException {


        FolderManager folderManager = new FolderManager();
        folderManager.makeConnection();
        folderManager.displayFolderNames();
        folderManager.readMessageFromFolders();
        folderManager.listMessageHeaders();
        folderManager.deleteMail();

    }
}

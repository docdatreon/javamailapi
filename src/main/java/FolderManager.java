import javax.mail.*;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class FolderManager extends Connection {

    protected Folder[] allFolders;
    protected String subject, subjectToDelete = "Document javaFX";
    protected String from;
    protected String to;
    protected String replyTo;
    protected Date sent;
    protected Message[] messages;
    protected Folder folder;

    @Override
    public String toString() {
        return "FolderManager{" +
                "allFolders=" + Arrays.toString(allFolders) +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", replyTo='" + replyTo + '\'' +
                ", subject='" + subject + '\'' +
                ", sent=" + sent +
                ", messages=" + Arrays.toString(messages) +
                ", folder=" + folder +
                '}';
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public String getSubject() {
        return subject;
    }

    public Date getSent() {
        return sent;
    }

    public void displayFolderNames() throws MessagingException {

        allFolders = store.getDefaultFolder().list("*");
        System.out.println("List of folders " + Arrays.asList(allFolders));
        System.out.println("Read content of folders - " + sender_id + " :");

        for (Folder folder : allFolders) {
            System.out.println(folder.getName());
        }
    }

    // method to read messages within different folders
    public void readMessageFromFolders() throws MessagingException {
        for (Folder folder : allFolders) {

            //folder mode to open
            folder.open(Folder.READ_ONLY);

            Message[] messages = folder.getMessages();
            System.out.println("Total messages in " + folder + '\t' + messages.length);
            if (messages.length != 0) {
                for (Message message : messages) {

                    // Get all the information from the message
                    from = InternetAddress.toString(message.getFrom());
                    if (from != null) {
                        System.out.println("From: " + from);
                    }
                    replyTo = InternetAddress.toString(message.getReplyTo());
                    if (replyTo != null) {
                        System.out.println("Reply-to: " + replyTo);
                    }
                    to = InternetAddress.toString(message.getRecipients(Message.RecipientType.TO));
                    if (to != null) {
                        System.out.println("To: " + to);
                    }
                    subject = message.getSubject();
                    if (subject != null) {
                        System.out.println("Subject: " + subject);
                    }
                    sent = message.getSentDate();
                    if (sent != null) {
                        System.out.println("Sent: " + sent);
                    }
                }
            }
        }
    }

    //method to get message details in arraylist
    public void listMessageHeaders() throws MessagingException {
        ArrayList<String> collect = new ArrayList();
        for (Folder folder : allFolders) {
            folder.open(Folder.READ_ONLY);

            Message[] messages = folder.getMessages();

            for (Message message : messages) {
                collect.add(message.getSubject());
                collect.add(String.valueOf(message.getFrom()));
                collect.add(String.valueOf(message.getReplyTo()));
                collect.add(String.valueOf(message.getRecipients(Message.RecipientType.TO)));
                collect.add(String.valueOf(message.getSentDate()));
            }
        }
        System.out.println(collect);
    }

    // method to delete mail
    public void deleteMail() throws MessagingException {

        Folder folderInbox = store.getFolder("INBOX");
        folderInbox.open(Folder.READ_WRITE);

        Message[] messages = folderInbox.getMessages();
        for (Message message : messages) {
            subject = message.getSubject();

            if (subject.contains(subjectToDelete)) {
                message.setFlag(Flags.Flag.DELETED, true);
                System.out.println("Marked DELETE for message: " + subject);
            }
        }

        folderInbox.close(true);
        //get message count of deleted message
        System.out.println(folderInbox.getDeletedMessageCount());
    }
}
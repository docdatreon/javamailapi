import com.sun.mail.iap.ConnectionException;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import java.util.Properties;

public class Connection {

    //specify the credentials accordingly
    protected String host = "md-38.webhostbox.net";
    protected String sender_id = "";
    protected String password = "";
    protected Store store;

    //method to check connection
    public void makeConnection() throws MessagingException, ConnectionException {

        Properties p = new Properties();

        Session session = Session.getInstance(p);

        store = session.getStore("imap");
        store.connect(host, sender_id, password);

        if (store == null) {
            throw new ConnectionException("Connection to incoming mail server not yet established");
        }

        else
            System.out.println("Connection Successful " + "\uD83D\uDC4D");
    }
}
